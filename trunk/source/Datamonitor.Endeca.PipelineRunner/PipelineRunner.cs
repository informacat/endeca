using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.Xml;
using Microsoft.ApplicationBlocks.Data;

namespace Datamonitor.Endeca
{
	/// <summary>
	/// This application is designed to perform an Endeca index update.
	/// It does this by processing an external xml based config file.
	/// This enables tasks, such as the running of stored procedures, or the alteration
	/// of Endeca Pipeline config files to take place prior to running the final Endeca script.
	/// </summary>
	class PipelineRunner
	{
		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main(string[] args)
		{
			try
			{
                // The first argument should specify the path of the external xml-based config file
                // It is an error to provide no path for the config file.
                if (0 == args.Length)
                    throw (new Exception("The path of the endecaConfig.xml file must be supplied as an argument."));
                
                // Open the configuration file
				FileInfo fi = new FileInfo(args[0].ToString());

				// Only proceed if the configuration file exists at the specified location
				if(fi.Exists)
				{
					XmlDocument endecaConfig = new XmlDocument();
					endecaConfig.Load(fi.FullName);

					// Serially process the nodes of the config file according to the node's type
					foreach(XmlNode node in endecaConfig.SelectNodes("/endecaConfig/*"))
					{
						// Cast to XmlElement for easier handling of attributes.
						XmlElement element = node as XmlElement;

						if(null!=element)
						{
							switch(element.LocalName)
							{
								case "storedProcedure":
									// Run the stored procedure specified in the elements attributes
									RunStoredProcedure(element);
									break;

								case "xmlUpdate":
									// Update the file and node specified in the elements attributes
									UpdateXmlFile(element);
									break;

								case "runCmd":
									// Synchronously perform the commands specified in the elements attributes at a command prompt
                                    RunCommand(element);
                                    break;

                                default:
                                    throw new Exception("The method or operation is not implemented.");
                                    break;
							}
						}
					}
				}
				else
                    throw (new Exception("The endecaConfig.xml file doesn't exist at the specified location."));
			}
			catch(Exception ex)
			{
                ExceptionPublisher.ProcessUnhandledException(ex);
			}
		}

        /// <summary>
        /// Run the specified command identified within the element checking exit codes unless specified otherwise.
        /// </summary>
        /// <param name="element">Configuration of command and handling of result.</param>
        private static void RunCommand(XmlElement element)
        {
            string processName = element.GetAttribute("cmd");
            bool ignoreExitCode = element.HasAttribute("ignoreExitCode") ? bool.Parse(element.GetAttribute("ignoreExitCode")) : false;

            using (Process proc = System.Diagnostics.Process.Start(Environment.GetEnvironmentVariable("COMSPEC"), string.Format(" /c {0}", processName)))
            {
                // Force synchronous behaviour, the process will only return on completion
                proc.WaitForExit();

                // Check exit code unless specified otherwise
                if (!ignoreExitCode && proc.ExitCode != 0)
                {
                    throw (new Exception(string.Format("The process {0} ended with return code {1}", processName, proc.ExitCode)));
                }
            }
        }

		/// <summary>
		/// This method runs the stored procedure specified in the "sp" attribute of the passed storedProcedure element.
		/// The connection string is also expected in the "connStr" attribute of the passed element.
		/// An optional "outputFile" attribute can be specified, in which case the results of the stored procedure will be saved to 
		/// the path specified in this attribute.
		/// Any stored procedure parameters are passed as param child elements with name and value attributes.
		/// </summary>
		/// <param name="element">XmlElement - A node with the localName of storedProcedure.</param>
		private static void RunStoredProcedure(XmlElement element)
		{
			// First retrieve attribute values
			string connStr = element.HasAttribute("connStr")?element.GetAttribute("connStr"):"";
			string sp = element.HasAttribute("sp")?element.GetAttribute("sp"):"";
			string outputFile = element.HasAttribute("outputFile")?element.GetAttribute("outputFile"):"";
			int cmdTimeout = element.HasAttribute("timeout")?int.Parse(element.GetAttribute("timeout")):0;
			XmlNodeList paramNodes = element.SelectNodes("param");

			// Create the connection and command
			using (SqlConnection conn = new SqlConnection(connStr))
			{
				using (SqlCommand cmd = conn.CreateCommand())
				{
					cmd.CommandText = sp;
					cmd.CommandType = CommandType.StoredProcedure;
					cmd.CommandTimeout = cmdTimeout;
			
					// Create and populate SQL parameters
					for(int i=0; i<paramNodes.Count; ++i)
						cmd.Parameters.AddWithValue(paramNodes[i].Attributes["name"].Value, paramNodes[i].Attributes["value"].Value);

					// Open the DB connection
					conn.Open();

					if(0==outputFile.Length)
						// No output file specified, just execute the procedure
						cmd.ExecuteNonQuery();
					else
					{
						// output file specified.  Save resulting SP XML to the file
						XmlReader x = cmd.ExecuteXmlReader();

						XmlDocument output = new XmlDocument();
						output.Load(x);
						FileInfo fi = new FileInfo(outputFile);
						output.Save(fi.FullName);
					}
				}
			}
		}

		/// <summary>
		/// This method first loads the external xml file specified in the passed element's "src" attribute.
		/// It then alters the value of the node specified in the "xPath" attribute to that in the "newValue"
		/// attribute and saves the amended file.
		/// </summary>
		/// <param name="element">XmlElement - A node with the localName of xmlUpdate</param>
		private static void UpdateXmlFile(XmlElement element)
		{
			// Retrieve the src attribute and construct a FileInfo class
			FileInfo filePath = new FileInfo(element.GetAttribute("src"));

			// Only proceed if the file exists.  If not raise an exception.
			if(filePath.Exists)
			{
				// Load the file, setting the XmlResolver property to null.  This is the same as setting resolveExternals
				// to false on a COM based XMLDomDocument, e.g. it won't attempt to load external DTD's.
				XmlDocument src = new XmlDocument();
				src.XmlResolver = null;
				src.Load(filePath.FullName);

				// Retrieve the node to change, and if it exists change it's value.
				// If it doesn't exist, raise an exception
				XmlNode nodeToChange = src.SelectSingleNode(element.GetAttribute("xPath"));
				if(null!=nodeToChange)
					nodeToChange.InnerText = element.GetAttribute("newValue");
				else
                    throw (new Exception("A node with the xPath specified, does not exist in the file identified by the src attribute."));

				// Once the amendment has been made, save the file.
				src.Save(filePath.FullName);
			}
			else
                throw (new Exception("The src attribute of the xmlUpdate element specifies a file that doesn't exist."));
		}
	}
}
