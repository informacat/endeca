@ECHO OFF
SETLOCAL

REM Read command line params
SET TargetDir=%~1
SET ConfigurationName=%~2

REM Remove leading and trailing character from params (quotes)
SET BuildSpecific=%~dp0%ConfigurationName%

:Main
REM Loop over all files (recursing into sub directories) copying build-specific ones over them where appropriate
FOR /R "%BuildSpecific%" %%f IN (*.*) DO (
	REM Test files and copy as needed
	CALL :ProcessFile "%%f" "%BuildSpecific%" "%TargetDir%"
)

GOTO End

:ProcessFile

SET _path=%~2

SET _sourceFile=%~f1
CALL SET RelativeFile=%%_sourceFile:%_path%\=%%
SET _destinationFile=%~3%RelativeFile%

SET _sourcePath=%~dp1
CALL SET RelativePath=%%_sourcePath:%_path%\=%%
SET _destinationPath=%~3%RelativePath%

REM If file already exists compare versions, otherwise write a copy
IF EXIST "%_destinationFile%" (
	FC "%_sourceFile%" "%_destinationFile%"
	IF ERRORLEVEL 1 (
		ECHO Files are not the same.
		ECHO Copying "%_sourceFile%" over "%_destinationFile%"
		COPY "%_sourceFile%" "%_destinationFile%" /Y
	)
) ELSE (
	ECHO Copying "%_sourceFile%" to "%_destinationFile%"
	MD "%_destinationPath%"
	COPY "%_sourceFile%" "%_destinationFile%"
)

:End

REM The end
ENDLOCAL
