using System;
using System.Configuration;
using System.Text;
using System.Net.Mail;

namespace Datamonitor.Endeca
{
	/// <summary>
	/// Summary description for ExceptionPublisher.
	/// </summary>
    public static class ExceptionPublisher
	{
        /// <summary>
        /// Process any unhandled exceptions that occur in the application.
        /// This code is called by all UI entry points in the application (e.g. button click events)
        /// when an unhandled exception occurs.
        /// You could also achieve this by handling the Application.ThreadException event, however
        /// the VS2005 debugger will break before this event is called.
        /// </summary>
        /// <param name="ex">The unhandled exception</param>
        public static void ProcessUnhandledException(Exception ex)
        {
            bool bEmail = Convert.ToBoolean(ConfigurationManager.AppSettings["logErrorsToEmail"]);

            if (bEmail)
            {
                SendErrorEmail(ex);
            }
        }

        private static void SendErrorEmail(Exception ex)
		{
			StringBuilder sbBody = new StringBuilder("\nSOURCE: ");

			sbBody.Append("Date Raised: ");
			sbBody.Append(DateTime.Now.ToString());
			sbBody.Append("\nMESSAGE: ");
			sbBody.Append(GetInnerExceptionMessages(ex));
			sbBody.Append("\nSTACKTRACE: ");
			sbBody.Append(ex.StackTrace);

            string strEmails = ConfigurationManager.AppSettings["emailAddresses"].ToString();
			
			if (strEmails.Length >0) 
			{
				string[] emails = strEmails.Split(Convert.ToChar("|"));
				
				MailMessage msg = new MailMessage();
				//msg.BodyFormat = MailFormat.Text;
				msg.To.Add(emails[0]);

				for(int i =1;i<emails.Length;i++)
					msg.CC.Add(emails[i]);

                msg.From = new MailAddress(ConfigurationManager.AppSettings["fromEmail"].ToString());
                msg.Subject = ConfigurationManager.AppSettings["errorSubject"].ToString(); 
				msg.Body = sbBody.ToString();
                string server = ConfigurationManager.AppSettings["smptServer"].ToString();
				SmtpClient _smtp = new SmtpClient(server);
				try
				{
                    _smtp.Send(msg);
				}
				catch
				{
					throw;
				}
			}
		}

        private static string GetInnerExceptionMessages(Exception e)
		{
			StringBuilder msg = new StringBuilder(string.Format("{0}\n", e.Message));
			
			while(e.InnerException!=null)
			{
				msg.Append(string.Format("{0}\n", e.InnerException.ToString()));
				e = e.InnerException;
			}
			
			return msg.ToString();
		}
    }
}
